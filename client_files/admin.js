var socket_prod = 'http://juanwolf.fr:8086/admin/';
var socket_dev = '127.0.0.1/admin/';

$(document).ready(function() {
        var socket = io.connect(socket_dev);
        console.log('socket= ' + socket);
        $(".form").submit(function() {
            if ($("#name").val() == 'juanlobo') {
                console.log("Admin connected successfully");
                socket.emit('adminConnected', {});
                console.log("Connection emitted");
            }
            return false;
        });
        socket.on('createChatRoom', function(data) {
            console.log("Creating new chat room with the id: " + data);
            addChatRoom(data, socket);
        });
        socket.on('messageForAdmin', function(data) {
            console.log("Message For Admin debug ----------------");
            console.log("date = " + JSON.stringify(data));
            addMessage(data);
            notifyAdmin(data);
        });
        socket.on('userDisconnected', function(id) {
            $("#"+ id).remove();
        });
});
var EventListenerAddedToRooms = false;
function addChatRoom(id, socket) {
    $('#content').append("<div class='chat-room' id='"+ id +"'>"
                + "<div class='message-container'> </div>"
                + "<form class='chat-message-form'>" 
                + "<input type='text' />"
                + "<input type='submit' value='envoyer' />"
                + "</form>" 
                + " </div>"
    );
    //if (!EventListenerAddedToRooms){
        $("#" + id + " .chat-message-form").submit(function() {
            $parent = $(this).parent(".chat-room");
            var id = $parent.attr("id");
            console.log("Message sending... to chat room n° " + id);
            var message = $(this).children(":first").val();
            console.log("Message sending... content: " + message);
            var data = {
                id: id,
                message: message,
            }
            console.log("Message object: " + JSON.stringify(data));
            socket.emit("adminMessageSent", data);
            console.log("Message sent !");
            addMessage({username:'Me', message:data.message, time: new Date(), id: id});
            return false;
        });
        EventListenerAddedToRooms = true;
    //}
}

function addMessage(data) {
    console.log("AddMessage debug ---------------------------------------------------");
    console.log("data received = " + JSON.stringify(data));
    var time = new Date(data.time);
    // Div
    var messageString = "<div class='message ";
    messageString += "'>";
    console.log("messageString div message = " + messageString);
    // Time
    messageString += "<span class='message-time'>";
    messageString +=  '' + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
    messageString += "</span>";
    console.log("messageString span time = " + messageString);
    // User
    messageString += "<p>";
    messageString += "<span class='message-username'>" + data.username +": " +  "</span>";
    // Message
    messageString += "<span class='message-content'>" + data.message + "</span>";
    messageString+= "</p>";
    messageString += "</div>";
    console.log("messageString = " + messageString);
    $("#" + data.id + "> .message-container").append(messageString);
    console.log("Addmessage end debug ----------------------------------------------");
}
function notifyAdmin(data) {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }

  // Let's check if the user is okay to get some notification
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    var notification = new Notification(data.username, {body: data.message});
  }

  // Otherwise, we need to ask the user for permission
  // Note, Chrome does not implement the permission static property
  // So we have to check for NOT 'denied' instead of 'default'
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {

      // Whatever the user answers, we make sure we store the information
      if(!('permission' in Notification)) {
        Notification.permission = permission;
      }

      // If the user is okay, let's create a notification
      if (permission === "granted") {
        var notification = new Notification(data.username, {body: data.message});
      }
    });
  }
}