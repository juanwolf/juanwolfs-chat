var socket_prod =  'http://juanwolf.fr:8086/chat/';
var socket_dev = 'http://localhost:8086/chat/';
window.onload = function() {
    $(".send-block :input").prop("disabled", true);
    var messages = [];
    var string_fr = {
        server : "Serveur",
        problem: "Il y a un problème :/",
        type_your_name : "Entrez votre nom !",
        robot_insult: "JE T'AI EU SALETÉ DE ROBOT, VA MOURIR EN ENFER !!!!!", 
        admin_not_connected: "Jean-Loup n'est pas connecté.",
        admin_connected: "Jean-Loup est connecté.",
    };
    var string_en = {
        server : "Server",
        problem: "There's a problem :/",
        type_your_name : "Please type your name !",
        robot_insult: "I GOT YOU BAD ROBOT, LET'S BURN IN HELL",
        admin_not_connected: "Jean-Loup is not connected.",
        admin_connected: "Jean-Loup is connected.",
    };
    var strings;
    var socket = io.connect(socket_dev);
    var field = document.getElementById("field");
    var sendButton = document.getElementById("send");
    var content = document.getElementById("content");
    var name = document.getElementById("name");
    var connectionButton = document.getElementById("connection");
    var trap = document.getElementById("trap");
    var adminConnected = false;
    var username;
    if (document.documentElement.lang === 'en') {
        strings = string_en;
    } else if (document.documentElement.lang === 'fr')  {
        strings = string_fr;
    }
    // User connected --------------------------------------------------------------------
    socket.on('join', function(data) {
        addMessage(data);
    });
    // Message reception -----------------------------------------------------------------
    socket.on('message', function (data) {
        console.log("Data received: " + JSON.stringify(data));
        if (data.message) {
            addMessage(data);
            scrollMessagesDivDown();
            if (data.username != username) {
               notifyUser(data);
            }
        } else {
            console.log(strings.problem, data);
        }
    });
    // Admin state ------------------------------------------------------------------------
    socket.on("adminState", function(data) {
        changeAdminState(data);
    });
    socket.on('adminConnected', function() {
        //addMessage({username: 'server', message:"Jean-Loup is now connected", time: new Date()});
        changeAdminState(true);
    });
    socket.on("adminDisconnected", function() {
        //addMessage({username: 'Server', message: 'Jean-Loup is now disconnected', time: new Date()});
        changeAdminState(false);
    });
    // connection button -----------------------------------------------------------------
    $('.form').submit(function() {
        if (trap.value !== '') {
            $("body").append(strings.robot_insult);
            return false;
        }
       if(name.value == "") {
                alert("Please type your name!");
            } else {
                username = name.value;
                data = {username: username};
                socket.emit('adduser', data);
                $(".send-block :input").prop("disabled", false);
                $(".form :input").prop("disabled", true);
            }
        return false;

    });
    // Send button -----------------------------------------------------------------------
    $(".send-block").submit(function() {
            var text = $("#field").val();
            field.value = "";
            data =  { message: text, username: name.value, time: new Date() };
            console.log("Data sent: " + JSON.stringify(data));
            socket.emit('send', data);
            if (!adminConnected) {
                console.log("Sending Mail To admin...");
                socket.emit('sendMailToAdmin', data);
            }
            
            return false;
    });
    /*
    Function which add a message inside de div#content.
    */
    function addMessage(data) {
        messages.push(data);
        console.log("AddMessage debug ---------------------------------------------------");
        console.log("data received = " + JSON.stringify(data));
        var time = new Date(data.time);
        // Div
        var messageString = "<div class='message ";
        if (data.username == username) {
            messageString += "own-message";
        }
        messageString += "'>";
        console.log("messageString div message = " + messageString);
        // Time
        messageString += "<span class='message-time'>";
        messageString +=  '' + time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
        messageString += "</span>";
        console.log("messageString span time = " + messageString);
        // User
        messageString += "<p>";
        messageString += "<span class='message-username'>" + data.username +": " +  "</span>";
        // Message
        messageString += "<span class='message-content'>" + data.message + "</span>";
        messageString+= "</p>";
        messageString += "</div>";
        console.log("messageString = " + messageString);
        $('#content').append(messageString);
        console.log("Addmessage end debug ----------------------------------------------");
    }
    /*
    Change the state of the administrator (Jean-Loup).
    */
    function changeAdminState(adminState) {
        adminConnected = adminState;
        $("#admin-state").empty();
        $("#admin-state").removeClass();
        $("#admin-state").append("<i class='fa fa-circle' alt='connction-circle'></i> ");
        if (adminConnected) {
            $("#admin-state").addClass('admin-connected');
            $("#admin-state").append(strings.admin_connected);
        } else {
            $("#admin-state").addClass('admin-disconnected')
            $("#admin-state").append(strings.admin_not_connected);
        }
    }
    $("#enter-event-checkbox").change(function() {
        console.log("Checkbox state changed");
        if ($('#enter-event-checkbox').is(':checked')) {
            $('#field').on('keyup', function(e) {
                if (e.which == 13 && !e.shiftKey) {
                    console.log("Enter pressed !!!!!");
                    $('.send-block').submit();
                    $("#field").val("");
                }
            });
        }
    });
}

function notifyUser(data) {
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    alert("This browser does not support desktop notification");
  }

  // Let's check if the user is okay to get some notification
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    var notification = new Notification("Jean-Loup", {icon:'/img/self-portrait-en.jpg',
        body: data.message});
  }

  // Otherwise, we need to ask the user for permission
  // Note, Chrome does not implement the permission static property
  // So we have to check for NOT 'denied' instead of 'default'
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {

      // Whatever the user answers, we make sure we store the information
      if(!('permission' in Notification)) {
        Notification.permission = permission;
      }

      // If the user is okay, let's create a notification
      if (permission === "granted") {
        var notification = new Notification("Jean-Loup", {body: data.message});
      }
    });
  }
}

function scrollMessagesDivDown() {
    console.log("#content height = " +  $("#content").height());
    $("#content").animate({ scrollTop: $(document).height() }, "slow");
    return false;
}