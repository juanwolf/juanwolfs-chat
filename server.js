// Settings
var express = require("express");
var nodemailer = require("nodemailer");
var transport = nodemailer.createTransport('direct', {debug: true});
var adminConnected = false;
var app = express();
var port = 8086;
var strings_en = {
    welcome: 'Welcome !',
    server: 'Server',
    leave: 'The user is gone'
};
var strings_fr = {
    welcome: 'Bienvenue ! ',
    server: 'Serveur',
    leave: "L'utilisateur est parti"
};
var strings;
var room_id = 0;
app.use(express.static("/home/juanwolf/juanwolf.fr"));
app.use(express.static(__dirname + "/client_files"));
app.set('views', __dirname + '/templates');
app.set('view engine', "jade");
app.engine('jade', require('jade').__express);

app.get("/", function(req, res) {
    var language = req.query.lang;
    if (language === "fr") {
        strings = strings_fr; 
        res.render("chat_fr");
    } else if(language === "en") {
        strings = strings_en;
        res.render("chat_en");
    }
});

app.get("/admin/", function (req, res) {
    res.render("admin");
});
var io = require('socket.io').listen(app.listen(port));
chat = io.of("/chat/");
admin = io.of("/admin/");

chat.on('connection', function (socket) {
    console.log("Connection received");
    socket.emit("adminState", adminConnected);
    socket.on('adduser', function(data) {
        room_id++;
        console.log("Adding user: " + data.username + "at room " + room_id);
        socket.username = data.username;
        socket.room = room_id;
        socket.join(room_id);
        if (adminConnected) {
            admin.emit("createChatRoom", room_id);
        }
        console.log("Request sent to admin");
        socket.emit('join', {username: strings.server ,message: strings.welcome, time: new Date()});

    });
    
    socket.on('send', function (data) {
        console.log("data received: " + JSON.stringify(data) + "at room " + socket.room);
        socket.emit('message', data);
        var admin_data = {
            username: data.username,
            message: data.message,
            id: ""+ socket.room,
            time: data.time,
        };

        socket.on('sendMailToAdmin', function(data) {
            console.log("Mail notification received !!!");
            sendMail(data, socket.room);
        });
        console.log("Message sent to admin : " + JSON.stringify(admin_data));
        admin.emit('messageForAdmin', admin_data);
    });

    socket.on("disconnect", function() {
        if (room_id > 0 && socket.room) {
            room_id--;
            admin.emit('userDisconnected', socket.room);
        }
    })
});


admin.on('connection', function (socket) {
    console.log("Someone detected on admin section");
    socket.on('adminConnected', function() {
        console.log("Admin connected.");
        adminConnected = true;
        chat.emit("adminConnected");
        for (var i = 0; i < room_id; i++) {
            socket.emit("createChatRoom", i+1);
        }

        socket.on('adminMessageSent', function(data) {
            console.log("Admin message received ! Content: " + JSON.stringify(data));
            var dataResponse =  {username: "Jean-Loup",
                                message: data.message,
                                time: new Date()
                               };
            console.log("Sending admin message to room n°" + data.id + "with content =" + JSON.stringify(data));
            chat.to(data.id).emit('message', dataResponse);
        });
    });
    socket.on("disconnect", function() {
        console.log("Admin disconnected");
        chat.emit("adminDisconnected");
        adminConnected = false;
    });
});
console.log("Listening on port " + port);
function sendMail(data, room) {
     // setup e-mail data with unicode symbols
    var mailOptions = {
        from: "Juanwolf's Server <juanwolf@juanwolf.fr>", // sender address
        to: "juanwolf@juanwolf.fr", // list of receivers
        subject: "You received a chat message from " + data.username, // Subject line
        text:  "In room "+ room + ", the message : " + data.message, // plaintext body
        html: "In room " + room + ", the message : <b> " +  data.message +"</b>" // html body
    }
    console.log("Options values : " + JSON.stringify(mailOptions));
    // send mail with defined transport object
    transport.sendMail(mailOptions, function(error, response){
        if(error){
            console.log(error);
        }else{
            console.log("Message sent: " + response.message);
        }
    });
}